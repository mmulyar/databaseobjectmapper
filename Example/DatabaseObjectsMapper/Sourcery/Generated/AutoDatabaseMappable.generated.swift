// Generated using Sourcery 2.2.5 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT
// MARK: - AutoDatabaseMappable
import RealmSwift
import DatabaseObjectsMapper
import DatabaseObjectsMapper

// swiftlint:disable file_length


// MARK: - AutoDatabaseMappable for classes, structs
// MARK: - TestCollectionsModel generated container
internal class TestCollectionsModelContainer: Object, UniqueDatabaseContainer {
    public static var idKey: WritableKeyPath<TestCollectionsModelContainer, Int> = \TestCollectionsModelContainer.id
    @Persisted(primaryKey: true) internal var id: Int
    @Persisted internal var strings: List<String>
    @Persisted internal var intValues: Data
    @Persisted internal var doubleValues: Data?
    @Persisted internal var dates: Data?
    @Persisted internal var codable: Data
    @Persisted internal var persistable: List<SomePersistable>
    @Persisted internal var urls: List<URL>
    @Persisted internal var dict: Map<String, SomePersistable>
    @Persisted internal var anotherDict: Data
    @Persisted internal var set: MutableSet<URL>
    @Persisted internal var anotherSet: Data
    @Persisted internal var someEnum: List<SomeEnum>
    @Persisted internal var someList: List<String>
    @Persisted internal var codableEnums: Data
    @Persisted internal var codableModels: Data
    @Persisted internal var optionalCodableModels: Data?
}

extension TestCollectionsModel: KeyPathConvertible {
    public static func key(for keyPath: PartialKeyPath<TestCollectionsModel>) -> String {
        switch keyPath {
        case \TestCollectionsModel.id: return "id"
        case \TestCollectionsModel.strings: return "strings"
        case \TestCollectionsModel.intValues: return "intValues"
        case \TestCollectionsModel.doubleValues: return "doubleValues"
        case \TestCollectionsModel.dates: return "dates"
        case \TestCollectionsModel.codable: return "codable"
        case \TestCollectionsModel.persistable: return "persistable"
        case \TestCollectionsModel.urls: return "urls"
        case \TestCollectionsModel.dict: return "dict"
        case \TestCollectionsModel.anotherDict: return "anotherDict"
        case \TestCollectionsModel.set: return "set"
        case \TestCollectionsModel.anotherSet: return "anotherSet"
        case \TestCollectionsModel.someEnum: return "someEnum"
        case \TestCollectionsModel.someList: return "someList"
        case \TestCollectionsModel.codableEnums: return "codableEnums"
        case \TestCollectionsModel.codableModels: return "codableModels"
        case \TestCollectionsModel.optionalCodableModels: return "optionalCodableModels"
        default:
            fatalError("Unhandled key path")
        }
    }

    enum CodingKeys: String, CodingKey {
        case id
        case strings
        case intValues
        case doubleValues
        case dates
        case codable
        case persistable
        case urls
        case dict
        case anotherDict
        case set
        case anotherSet
        case someEnum
        case someList
        case codableEnums
        case codableModels
        case optionalCodableModels
    }
}

internal extension TestCollectionsModel {
    static func mappable(for container: Container) throws -> Self {
        TestCollectionsModel(
            id: container.id,
            strings: Array(container.strings),
            intValues: try .create(with: container.intValues, using: .realmDecoder)!,
            doubleValues: try .create(with: container.doubleValues, using: .realmDecoder),
            dates: try .create(with: container.dates, using: .realmDecoder),
            codable: try .create(with: container.codable, using: .realmDecoder)!,
            persistable: Array(container.persistable),
            urls: Array(container.urls),
            dict: Dictionary(uniqueKeysWithValues: zip(container.dict.keys, container.dict.values)),
            anotherDict: try .create(with: container.anotherDict, using: .realmDecoder)!,
            set: Set(container.set),
            anotherSet: try .create(with: container.anotherSet, using: .realmDecoder)!,
            someEnum: Array(container.someEnum),
            someList: Array(container.someList),
            codableEnums: try .create(with: container.codableEnums, using: .realmDecoder)!,
            codableModels: try .create(with: container.codableModels, using: .realmDecoder)!,
            optionalCodableModels: try .create(with: container.optionalCodableModels, using: .realmDecoder)
        )
    }
    func updateContainerRelations(_ container: Container) {
    }
    func updateContainerSkippingRelations(_ container: Container) {
        container.strings.removeAll()
        container.strings.append(objectsIn: strings)
        container.intValues = intValues.binaryValue(with: .realmEncoder)
        container.doubleValues = doubleValues?.binaryValue(with: .realmEncoder)
        container.dates = dates?.binaryValue(with: .realmEncoder)
        container.codable = codable.binaryValue(with: .realmEncoder)
        container.persistable.removeAll()
        container.persistable.append(objectsIn: persistable)
        container.urls.removeAll()
        container.urls.append(objectsIn: urls)
        container.dict.removeAll()
        container.dict.merge(dict, uniquingKeysWith: { _, v in v })
        container.anotherDict = anotherDict.binaryValue(with: .realmEncoder)
        container.set.removeAll()
        container.set.insert(objectsIn: set)
        container.anotherSet = anotherSet.binaryValue(with: .realmEncoder)
        container.someEnum.removeAll()
        container.someEnum.append(objectsIn: someEnum)
        container.someList.removeAll()
        container.someList.append(objectsIn: someList)
        container.codableEnums = codableEnums.binaryValue(with: .realmEncoder)
        container.codableModels = codableModels.binaryValue(with: .realmEncoder)
        container.optionalCodableModels = optionalCodableModels?.binaryValue(with: .realmEncoder)
    }
    func key(for relation: Relation<some UniquelyMappable>) -> String? {
        return nil
    }
    func key(for relation: EmbeddedRelation<some DatabaseMappable>) -> String? {
        return nil
    }
}
// MARK: - TestDateModel generated container
internal class TestDateModelContainer: Object, UniqueDatabaseContainer {
    public static var idKey: WritableKeyPath<TestDateModelContainer, Int> = \TestDateModelContainer.id
    @Persisted(primaryKey: true) internal var id: Int
    @Persisted internal var date: Date
}

extension TestDateModel: KeyPathConvertible {
    public static func key(for keyPath: PartialKeyPath<TestDateModel>) -> String {
        switch keyPath {
        case \TestDateModel.id: return "id"
        case \TestDateModel.date: return "date"
        default:
            fatalError("Unhandled key path")
        }
    }

    enum CodingKeys: String, CodingKey {
        case id
        case date
    }
}
// MARK: - TestERRModel generated container
internal class TestERRModelContainer: EmbeddedObject, DatabaseContainer {
    @Persisted internal var name: String
    @Persisted internal var someCount: Int
    @Persisted internal var url: URL?
}

extension TestERRModel: KeyPathConvertible {
    public static func key(for keyPath: PartialKeyPath<TestERRModel>) -> String {
        switch keyPath {
        case \TestERRModel.name: return "name"
        case \TestERRModel.someCount: return "someCount"
        case \TestERRModel.url: return "url"
        default:
            fatalError("Unhandled key path")
        }
    }

    enum CodingKeys: String, CodingKey {
        case name
        case someCount
        case url
    }
}
// MARK: - TestPrimitivesModel generated container
internal class TestPrimitivesModelContainer: Object, UniqueDatabaseContainer {
    public static var idKey: WritableKeyPath<TestPrimitivesModelContainer, Int> = \TestPrimitivesModelContainer.id
    @Persisted(primaryKey: true) internal var id: Int
    @Persisted internal var value: Int32?
    @Persisted internal var doubleValue: Double
    @Persisted var decimalValue: Decimal128
    @Persisted internal var floatValue: Float?
    @Persisted internal var boolValue: Bool?
    @Persisted internal var urlValue: URL?
    @Persisted var decimalValueOpt: Decimal128?
    @Persisted internal var someEnum: SomeEnum
    @Persisted internal var someEnumOpt: SomeEnum?
    @Persisted internal var stringEnum: String
    @Persisted internal var stringEnumOpt: String?
    @Persisted internal var someComplexCodable: Data?
}

extension TestPrimitivesModel: KeyPathConvertible {
    public static func key(for keyPath: PartialKeyPath<TestPrimitivesModel>) -> String {
        switch keyPath {
        case \TestPrimitivesModel.id: return "id"
        case \TestPrimitivesModel.value: return "value"
        case \TestPrimitivesModel.doubleValue: return "doubleValue"
        case \TestPrimitivesModel.decimalValue: return "decimalValue"
        case \TestPrimitivesModel.floatValue: return "floatValue"
        case \TestPrimitivesModel.boolValue: return "boolValue"
        case \TestPrimitivesModel.urlValue: return "urlValue"
        case \TestPrimitivesModel.decimalValueOpt: return "decimalValueOpt"
        case \TestPrimitivesModel.someEnum: return "someEnum"
        case \TestPrimitivesModel.someEnumOpt: return "someEnumOpt"
        case \TestPrimitivesModel.stringEnum: return "stringEnum"
        case \TestPrimitivesModel.stringEnumOpt: return "stringEnumOpt"
        case \TestPrimitivesModel.someComplexCodable: return "someComplexCodable"
        default:
            fatalError("Unhandled key path")
        }
    }

    enum CodingKeys: String, CodingKey {
        case id
        case value
        case doubleValue
        case decimalValue
        case floatValue
        case boolValue
        case urlValue
        case decimalValueOpt
        case someEnum
        case someEnumOpt
        case stringEnum
        case stringEnumOpt
        case someComplexCodable
    }
}

internal extension TestPrimitivesModel {
    static func mappable(for container: Container) throws -> Self {
        TestPrimitivesModel(
            id: container.id,
            value: container.value,
            doubleValue: container.doubleValue,
            decimalValue: Decimal(string: container.decimalValue.stringValue)!,
            floatValue: container.floatValue,
            boolValue: container.boolValue,
            urlValue: container.urlValue,
            decimalValueOpt: Decimal(string: container.decimalValueOpt?.stringValue ?? ""),
            someEnum: container.someEnum,
            someEnumOpt: container.someEnumOpt,
            stringEnum: .init(rawValue: container.stringEnum)!,
            stringEnumOpt: container.stringEnumOpt.flatMap { .init(rawValue: $0) },
            someComplexCodable: try .create(with: container.someComplexCodable, using: .realmDecoder)
        )
    }
    func updateContainerRelations(_ container: Container) {
    }
    func updateContainerSkippingRelations(_ container: Container) {
        container.value = value
        container.doubleValue = doubleValue
        container.decimalValue = Decimal128(value: decimalValue)
        container.floatValue = floatValue
        container.boolValue = boolValue
        container.urlValue = urlValue
        container.decimalValueOpt = decimalValueOpt.flatMap { Decimal128(value: $0) }
        container.someEnum = someEnum
        container.someEnumOpt = someEnumOpt
        container.stringEnum = stringEnum.rawValue
        container.stringEnumOpt = stringEnumOpt?.rawValue
        container.someComplexCodable = someComplexCodable?.binaryValue(with: .realmEncoder)
    }
    func key(for relation: Relation<some UniquelyMappable>) -> String? {
        return nil
    }
    func key(for relation: EmbeddedRelation<some DatabaseMappable>) -> String? {
        return nil
    }
}
// MARK: - TestRNModel generated container
internal class TestRNModelContainer: Object, UniqueDatabaseContainer {
    public static var idKey: WritableKeyPath<TestRNModelContainer, Int> = \TestRNModelContainer.id
    @Persisted(primaryKey: true) internal var id: Int
    @Persisted internal var name: String
    @Persisted internal var owner: TestSomeModelContainer?
}

extension TestRNModel: KeyPathConvertible {
    public static func key(for keyPath: PartialKeyPath<TestRNModel>) -> String {
        switch keyPath {
        case \TestRNModel.id: return "id"
        case \TestRNModel.name: return "name"
        case \TestRNModel.owner: return "owner"
        default:
            fatalError("Unhandled key path")
        }
    }

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case owner
    }
}

internal extension TestRNModel {
    static func mappable(for container: Container) throws -> Self {
        TestRNModel(
            id: container.id,
            name: container.name,
            owner: try TestSomeModel.mappable(for: container.owner!)
        )
    }
    func updateContainerRelations(_ container: Container) {
        container.owner = try! owner.container(with: container.realm)
    }
    func updateContainerSkippingRelations(_ container: Container) {
        container.name = name
    }
    func key(for relation: Relation<some UniquelyMappable>) -> String? {
        return nil
    }
    func key(for relation: EmbeddedRelation<some DatabaseMappable>) -> String? {
        return nil
    }
}
// MARK: - TestRRModel generated container
internal class TestRRModelContainer: Object, UniqueDatabaseContainer {
    public static var idKey: WritableKeyPath<TestRRModelContainer, Int> = \TestRRModelContainer.id
    @Persisted(primaryKey: true) internal var id: Int
    @Persisted internal var name: String
    @Persisted internal var owner: TestSomeModelContainer?
    @Persisted internal var user: TestERRModelContainer?
    @Persisted internal var users: List<TestRRModelContainer>
    @Persisted internal var owners: List<TestERRModelContainer>
}

extension TestRRModel: KeyPathConvertible {
    public static func key(for keyPath: PartialKeyPath<TestRRModel>) -> String {
        switch keyPath {
        case \TestRRModel.id: return "id"
        case \TestRRModel.name: return "name"
        case \TestRRModel.owner: return "owner"
        case \TestRRModel.user: return "user"
        case \TestRRModel.users: return "users"
        case \TestRRModel.owners: return "owners"
        default:
            fatalError("Unhandled key path")
        }
    }

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case owner
        case user
    }
}

internal extension TestRRModel {
    static func mappable(for container: Container) throws -> Self {
        TestRRModel(
            id: container.id,
            name: container.name,
            owner: try container.owner.flatMap { try TestSomeModel.mappable(for: $0) },
            user: try container.user.flatMap { try TestERRModel.mappable(for: $0) }
        )
    }
    func updateContainerRelations(_ container: Container) {
        container.owner = try? owner?.container(with: container.realm)
        container.user = try? user?.container(with: container.realm)
    }
    func updateContainerSkippingRelations(_ container: Container) {
        container.name = name
    }
    func key(for relation: Relation<some UniquelyMappable>) -> String? {
        if users === relation {
            return "users"
        }
        return nil
    }
    func key(for relation: EmbeddedRelation<some DatabaseMappable>) -> String? {
        if owners === relation {
            return "owners"
        }
        return nil
    }
}
// MARK: - TestSomeModel generated container
internal class TestSomeModelContainer: Object, UniqueDatabaseContainer {
    public static var idKey: WritableKeyPath<TestSomeModelContainer, Int> = \TestSomeModelContainer.userId
    @Persisted(primaryKey: true) internal var userId: Int
    @Persisted(indexed: true) internal var userName: String
    @Persisted internal var userAvatar: String
    @Persisted(indexed: true) internal var title: String?
    @Persisted internal var count: Int
    @Persisted(originProperty: "owner") internal var inverseModel: LinkingObjects<TestRRModelContainer>
    @Persisted internal var directModels: List<TestRRModelContainer>
    @Persisted internal var nestedModel: Data?
}

extension TestSomeModel: KeyPathConvertible {
    public static func key(for keyPath: PartialKeyPath<TestSomeModel>) -> String {
        switch keyPath {
        case \TestSomeModel.userId: return "userId"
        case \TestSomeModel.userName: return "userName"
        case \TestSomeModel.userAvatar: return "userAvatar"
        case \TestSomeModel.title: return "title"
        case \TestSomeModel.count: return "count"
        case \TestSomeModel.inverseModel: return "inverseModel"
        case \TestSomeModel.directModels: return "directModels"
        case \TestSomeModel.nestedModel: return "nestedModel"
        default:
            fatalError("Unhandled key path")
        }
    }

    enum CodingKeys: String, CodingKey {
        case userId
        case userName
        case userAvatar
        case title
        case count
        case nestedModel
    }
}

internal extension TestSomeModel {
    static func mappable(for container: Container) throws -> Self {
        TestSomeModel(
            userId: container.userId,
            userName: container.userName,
            userAvatar: container.userAvatar,
            title: container.title,
            count: container.count,
            nestedModel: try .create(with: container.nestedModel, using: .realmDecoder)
        )
    }
    func updateContainerRelations(_ container: Container) {
    }
    func updateContainerSkippingRelations(_ container: Container) {
        container.userName = userName
        container.userAvatar = userAvatar
        container.title = title
        container.count = count
        container.nestedModel = nestedModel?.binaryValue(with: .realmEncoder)
    }
    func key(for relation: Relation<some UniquelyMappable>) -> String? {
        if inverseModel === relation {
            return "inverseModel"
        }
        if directModels === relation {
            return "directModels"
        }
        return nil
    }
    func key(for relation: EmbeddedRelation<some DatabaseMappable>) -> String? {
        return nil
    }
}
