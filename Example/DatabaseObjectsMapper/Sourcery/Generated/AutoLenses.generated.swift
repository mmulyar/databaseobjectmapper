// Generated using Sourcery 2.2.5 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT
// MARK: - AutoLenses
import Foundation
import DatabaseObjectsMapper

// swiftlint:disable line_length
infix operator *~: MultiplicationPrecedence
infix operator |>: AdditionPrecedence

//Lenses API
struct Lens<Whole, Part> {
    let get: (Whole) -> Part
    let set: (Part, Whole) -> Whole
}

func * <A, B, C> (lhs: Lens<A, B>, rhs: Lens<B, C>) -> Lens<A, C> {
    return Lens<A, C>(
        get: { a in rhs.get(lhs.get(a)) },
        set: { (c, a) in lhs.set(rhs.set(c, lhs.get(a)), a) }
    )
}

func *~ <A, B> (lhs: Lens<A, B>, rhs: B) -> (A) -> A {
    return { a in lhs.set(rhs, a) }
}

func |> <A, B> (x: A, f: (A) -> B) -> B {
    return f(x)
}

func |> <A, B, C> (f: @escaping (A) -> B, g: @escaping (B) -> C) -> (A) -> C {
    return { g(f($0)) }
}

// Bound lenses API
struct BoundLens<Whole, Part> {
    let instance: Whole
    let lens: Lens<Whole, Part>

    func get() -> Part {
        return lens.get(instance)
    }
    func set(_ newPart: Part) -> Whole {
        return lens.set(newPart, instance)
    }
}

extension TestCDModel {
    static var idLens: Lens<TestCDModel, Int> = {
        Lens<TestCDModel, Int>(
            get: { $0.id },
            set: { id, _value in
                TestCDModel(id: id, index: _value.index, name: _value.name, count: _value.count, someCount: _value.someCount, urls: _value.urls, subModel: _value.subModel)
            }
        )
    }()
    var idLens: BoundLens<TestCDModel, Int> {
        BoundLens<TestCDModel, Int>(instance: self, lens: TestCDModel.idLens)
    }
    static var indexLens: Lens<TestCDModel, Int?> = {
        Lens<TestCDModel, Int?>(
            get: { $0.index },
            set: { index, _value in
                TestCDModel(id: _value.id, index: index, name: _value.name, count: _value.count, someCount: _value.someCount, urls: _value.urls, subModel: _value.subModel)
            }
        )
    }()
    var indexLens: BoundLens<TestCDModel, Int?> {
        BoundLens<TestCDModel, Int?>(instance: self, lens: TestCDModel.indexLens)
    }
    static var nameLens: Lens<TestCDModel, String> = {
        Lens<TestCDModel, String>(
            get: { $0.name },
            set: { name, _value in
                TestCDModel(id: _value.id, index: _value.index, name: name, count: _value.count, someCount: _value.someCount, urls: _value.urls, subModel: _value.subModel)
            }
        )
    }()
    var nameLens: BoundLens<TestCDModel, String> {
        BoundLens<TestCDModel, String>(instance: self, lens: TestCDModel.nameLens)
    }
    static var countLens: Lens<TestCDModel, Int> = {
        Lens<TestCDModel, Int>(
            get: { $0.count },
            set: { count, _value in
                TestCDModel(id: _value.id, index: _value.index, name: _value.name, count: count, someCount: _value.someCount, urls: _value.urls, subModel: _value.subModel)
            }
        )
    }()
    var countLens: BoundLens<TestCDModel, Int> {
        BoundLens<TestCDModel, Int>(instance: self, lens: TestCDModel.countLens)
    }
    static var someCountLens: Lens<TestCDModel, Int> = {
        Lens<TestCDModel, Int>(
            get: { $0.someCount },
            set: { someCount, _value in
                TestCDModel(id: _value.id, index: _value.index, name: _value.name, count: _value.count, someCount: someCount, urls: _value.urls, subModel: _value.subModel)
            }
        )
    }()
    var someCountLens: BoundLens<TestCDModel, Int> {
        BoundLens<TestCDModel, Int>(instance: self, lens: TestCDModel.someCountLens)
    }
    static var urlsLens: Lens<TestCDModel, [URL]?> = {
        Lens<TestCDModel, [URL]?>(
            get: { $0.urls },
            set: { urls, _value in
                TestCDModel(id: _value.id, index: _value.index, name: _value.name, count: _value.count, someCount: _value.someCount, urls: urls, subModel: _value.subModel)
            }
        )
    }()
    var urlsLens: BoundLens<TestCDModel, [URL]?> {
        BoundLens<TestCDModel, [URL]?>(instance: self, lens: TestCDModel.urlsLens)
    }
    static var subModelLens: Lens<TestCDModel, Relation<TestSomeModel>> = {
        Lens<TestCDModel, Relation<TestSomeModel>>(
            get: { $0.subModel },
            set: { subModel, _value in
                TestCDModel(id: _value.id, index: _value.index, name: _value.name, count: _value.count, someCount: _value.someCount, urls: _value.urls, subModel: subModel)
            }
        )
    }()
    var subModelLens: BoundLens<TestCDModel, Relation<TestSomeModel>> {
        BoundLens<TestCDModel, Relation<TestSomeModel>>(instance: self, lens: TestCDModel.subModelLens)
    }
}
extension TestCDSimpleModel {
    static var titleLens: Lens<TestCDSimpleModel, String> = {
        Lens<TestCDSimpleModel, String>(
            get: { $0.title },
            set: { title, _value in
                TestCDSimpleModel(title: title, count: _value.count)
            }
        )
    }()
    var titleLens: BoundLens<TestCDSimpleModel, String> {
        BoundLens<TestCDSimpleModel, String>(instance: self, lens: TestCDSimpleModel.titleLens)
    }
    static var countLens: Lens<TestCDSimpleModel, Int> = {
        Lens<TestCDSimpleModel, Int>(
            get: { $0.count },
            set: { count, _value in
                TestCDSimpleModel(title: _value.title, count: count)
            }
        )
    }()
    var countLens: BoundLens<TestCDSimpleModel, Int> {
        BoundLens<TestCDSimpleModel, Int>(instance: self, lens: TestCDSimpleModel.countLens)
    }
}
extension TestCollectionsModel {
    static var idLens: Lens<TestCollectionsModel, Int> = {
        Lens<TestCollectionsModel, Int>(
            get: { $0.id },
            set: { id, _value in
                TestCollectionsModel(id: id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var idLens: BoundLens<TestCollectionsModel, Int> {
        BoundLens<TestCollectionsModel, Int>(instance: self, lens: TestCollectionsModel.idLens)
    }
    static var stringsLens: Lens<TestCollectionsModel, [String]> = {
        Lens<TestCollectionsModel, [String]>(
            get: { $0.strings },
            set: { strings, _value in
                TestCollectionsModel(id: _value.id, strings: strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var stringsLens: BoundLens<TestCollectionsModel, [String]> {
        BoundLens<TestCollectionsModel, [String]>(instance: self, lens: TestCollectionsModel.stringsLens)
    }
    static var intValuesLens: Lens<TestCollectionsModel, [Int64?]> = {
        Lens<TestCollectionsModel, [Int64?]>(
            get: { $0.intValues },
            set: { intValues, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var intValuesLens: BoundLens<TestCollectionsModel, [Int64?]> {
        BoundLens<TestCollectionsModel, [Int64?]>(instance: self, lens: TestCollectionsModel.intValuesLens)
    }
    static var doubleValuesLens: Lens<TestCollectionsModel, [Double]?> = {
        Lens<TestCollectionsModel, [Double]?>(
            get: { $0.doubleValues },
            set: { doubleValues, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var doubleValuesLens: BoundLens<TestCollectionsModel, [Double]?> {
        BoundLens<TestCollectionsModel, [Double]?>(instance: self, lens: TestCollectionsModel.doubleValuesLens)
    }
    static var datesLens: Lens<TestCollectionsModel, [Date]?> = {
        Lens<TestCollectionsModel, [Date]?>(
            get: { $0.dates },
            set: { dates, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var datesLens: BoundLens<TestCollectionsModel, [Date]?> {
        BoundLens<TestCollectionsModel, [Date]?>(instance: self, lens: TestCollectionsModel.datesLens)
    }
    static var codableLens: Lens<TestCollectionsModel, [SomeCodable]> = {
        Lens<TestCollectionsModel, [SomeCodable]>(
            get: { $0.codable },
            set: { codable, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var codableLens: BoundLens<TestCollectionsModel, [SomeCodable]> {
        BoundLens<TestCollectionsModel, [SomeCodable]>(instance: self, lens: TestCollectionsModel.codableLens)
    }
    static var persistableLens: Lens<TestCollectionsModel, [SomePersistable]> = {
        Lens<TestCollectionsModel, [SomePersistable]>(
            get: { $0.persistable },
            set: { persistable, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var persistableLens: BoundLens<TestCollectionsModel, [SomePersistable]> {
        BoundLens<TestCollectionsModel, [SomePersistable]>(instance: self, lens: TestCollectionsModel.persistableLens)
    }
    static var urlsLens: Lens<TestCollectionsModel, [URL]> = {
        Lens<TestCollectionsModel, [URL]>(
            get: { $0.urls },
            set: { urls, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var urlsLens: BoundLens<TestCollectionsModel, [URL]> {
        BoundLens<TestCollectionsModel, [URL]>(instance: self, lens: TestCollectionsModel.urlsLens)
    }
    static var dictLens: Lens<TestCollectionsModel, [String: SomePersistable]> = {
        Lens<TestCollectionsModel, [String: SomePersistable]>(
            get: { $0.dict },
            set: { dict, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var dictLens: BoundLens<TestCollectionsModel, [String: SomePersistable]> {
        BoundLens<TestCollectionsModel, [String: SomePersistable]>(instance: self, lens: TestCollectionsModel.dictLens)
    }
    static var anotherDictLens: Lens<TestCollectionsModel, [SomeCodable: SomeStringEnum]> = {
        Lens<TestCollectionsModel, [SomeCodable: SomeStringEnum]>(
            get: { $0.anotherDict },
            set: { anotherDict, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var anotherDictLens: BoundLens<TestCollectionsModel, [SomeCodable: SomeStringEnum]> {
        BoundLens<TestCollectionsModel, [SomeCodable: SomeStringEnum]>(instance: self, lens: TestCollectionsModel.anotherDictLens)
    }
    static var setLens: Lens<TestCollectionsModel, Set<URL>> = {
        Lens<TestCollectionsModel, Set<URL>>(
            get: { $0.set },
            set: { set, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var setLens: BoundLens<TestCollectionsModel, Set<URL>> {
        BoundLens<TestCollectionsModel, Set<URL>>(instance: self, lens: TestCollectionsModel.setLens)
    }
    static var anotherSetLens: Lens<TestCollectionsModel, Set<SomeCodable?>> = {
        Lens<TestCollectionsModel, Set<SomeCodable?>>(
            get: { $0.anotherSet },
            set: { anotherSet, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var anotherSetLens: BoundLens<TestCollectionsModel, Set<SomeCodable?>> {
        BoundLens<TestCollectionsModel, Set<SomeCodable?>>(instance: self, lens: TestCollectionsModel.anotherSetLens)
    }
    static var someEnumLens: Lens<TestCollectionsModel, [SomeEnum]> = {
        Lens<TestCollectionsModel, [SomeEnum]>(
            get: { $0.someEnum },
            set: { someEnum, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var someEnumLens: BoundLens<TestCollectionsModel, [SomeEnum]> {
        BoundLens<TestCollectionsModel, [SomeEnum]>(instance: self, lens: TestCollectionsModel.someEnumLens)
    }
    static var someListLens: Lens<TestCollectionsModel, [String]> = {
        Lens<TestCollectionsModel, [String]>(
            get: { $0.someList },
            set: { someList, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var someListLens: BoundLens<TestCollectionsModel, [String]> {
        BoundLens<TestCollectionsModel, [String]>(instance: self, lens: TestCollectionsModel.someListLens)
    }
    static var codableEnumsLens: Lens<TestCollectionsModel, [Link]> = {
        Lens<TestCollectionsModel, [Link]>(
            get: { $0.codableEnums },
            set: { codableEnums, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: codableEnums, codableModels: _value.codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var codableEnumsLens: BoundLens<TestCollectionsModel, [Link]> {
        BoundLens<TestCollectionsModel, [Link]>(instance: self, lens: TestCollectionsModel.codableEnumsLens)
    }
    static var codableModelsLens: Lens<TestCollectionsModel, [TestERRModel]> = {
        Lens<TestCollectionsModel, [TestERRModel]>(
            get: { $0.codableModels },
            set: { codableModels, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: codableModels, optionalCodableModels: _value.optionalCodableModels)
            }
        )
    }()
    var codableModelsLens: BoundLens<TestCollectionsModel, [TestERRModel]> {
        BoundLens<TestCollectionsModel, [TestERRModel]>(instance: self, lens: TestCollectionsModel.codableModelsLens)
    }
    static var optionalCodableModelsLens: Lens<TestCollectionsModel, [TestERRModel]?> = {
        Lens<TestCollectionsModel, [TestERRModel]?>(
            get: { $0.optionalCodableModels },
            set: { optionalCodableModels, _value in
                TestCollectionsModel(id: _value.id, strings: _value.strings, intValues: _value.intValues, doubleValues: _value.doubleValues, dates: _value.dates, codable: _value.codable, persistable: _value.persistable, urls: _value.urls, dict: _value.dict, anotherDict: _value.anotherDict, set: _value.set, anotherSet: _value.anotherSet, someEnum: _value.someEnum, someList: _value.someList, codableEnums: _value.codableEnums, codableModels: _value.codableModels, optionalCodableModels: optionalCodableModels)
            }
        )
    }()
    var optionalCodableModelsLens: BoundLens<TestCollectionsModel, [TestERRModel]?> {
        BoundLens<TestCollectionsModel, [TestERRModel]?>(instance: self, lens: TestCollectionsModel.optionalCodableModelsLens)
    }
}
extension TestDateModel {
    static var idLens: Lens<TestDateModel, Int> = {
        Lens<TestDateModel, Int>(
            get: { $0.id },
            set: { id, _value in
                TestDateModel(id: id, date: _value.date)
            }
        )
    }()
    var idLens: BoundLens<TestDateModel, Int> {
        BoundLens<TestDateModel, Int>(instance: self, lens: TestDateModel.idLens)
    }
    static var dateLens: Lens<TestDateModel, Date> = {
        Lens<TestDateModel, Date>(
            get: { $0.date },
            set: { date, _value in
                TestDateModel(id: _value.id, date: date)
            }
        )
    }()
    var dateLens: BoundLens<TestDateModel, Date> {
        BoundLens<TestDateModel, Date>(instance: self, lens: TestDateModel.dateLens)
    }
}
extension TestERRModel {
    static var nameLens: Lens<TestERRModel, String> = {
        Lens<TestERRModel, String>(
            get: { $0.name },
            set: { name, _value in
                TestERRModel(name: name, someCount: _value.someCount, url: _value.url)
            }
        )
    }()
    var nameLens: BoundLens<TestERRModel, String> {
        BoundLens<TestERRModel, String>(instance: self, lens: TestERRModel.nameLens)
    }
    static var someCountLens: Lens<TestERRModel, Int> = {
        Lens<TestERRModel, Int>(
            get: { $0.someCount },
            set: { someCount, _value in
                TestERRModel(name: _value.name, someCount: someCount, url: _value.url)
            }
        )
    }()
    var someCountLens: BoundLens<TestERRModel, Int> {
        BoundLens<TestERRModel, Int>(instance: self, lens: TestERRModel.someCountLens)
    }
    static var urlLens: Lens<TestERRModel, URL?> = {
        Lens<TestERRModel, URL?>(
            get: { $0.url },
            set: { url, _value in
                TestERRModel(name: _value.name, someCount: _value.someCount, url: url)
            }
        )
    }()
    var urlLens: BoundLens<TestERRModel, URL?> {
        BoundLens<TestERRModel, URL?>(instance: self, lens: TestERRModel.urlLens)
    }
}
extension TestModel {
    static var idLens: Lens<TestModel, Int> = {
        Lens<TestModel, Int>(
            get: { $0.id },
            set: { id, _value in
                TestModel(id: id, index: _value.index, name: _value.name, count: _value.count, someCount: _value.someCount, urls: _value.urls, someModel: _value.someModel)
            }
        )
    }()
    var idLens: BoundLens<TestModel, Int> {
        BoundLens<TestModel, Int>(instance: self, lens: TestModel.idLens)
    }
    static var indexLens: Lens<TestModel, Int?> = {
        Lens<TestModel, Int?>(
            get: { $0.index },
            set: { index, _value in
                TestModel(id: _value.id, index: index, name: _value.name, count: _value.count, someCount: _value.someCount, urls: _value.urls, someModel: _value.someModel)
            }
        )
    }()
    var indexLens: BoundLens<TestModel, Int?> {
        BoundLens<TestModel, Int?>(instance: self, lens: TestModel.indexLens)
    }
    static var nameLens: Lens<TestModel, String> = {
        Lens<TestModel, String>(
            get: { $0.name },
            set: { name, _value in
                TestModel(id: _value.id, index: _value.index, name: name, count: _value.count, someCount: _value.someCount, urls: _value.urls, someModel: _value.someModel)
            }
        )
    }()
    var nameLens: BoundLens<TestModel, String> {
        BoundLens<TestModel, String>(instance: self, lens: TestModel.nameLens)
    }
    static var countLens: Lens<TestModel, Int> = {
        Lens<TestModel, Int>(
            get: { $0.count },
            set: { count, _value in
                TestModel(id: _value.id, index: _value.index, name: _value.name, count: count, someCount: _value.someCount, urls: _value.urls, someModel: _value.someModel)
            }
        )
    }()
    var countLens: BoundLens<TestModel, Int> {
        BoundLens<TestModel, Int>(instance: self, lens: TestModel.countLens)
    }
    static var someCountLens: Lens<TestModel, Int> = {
        Lens<TestModel, Int>(
            get: { $0.someCount },
            set: { someCount, _value in
                TestModel(id: _value.id, index: _value.index, name: _value.name, count: _value.count, someCount: someCount, urls: _value.urls, someModel: _value.someModel)
            }
        )
    }()
    var someCountLens: BoundLens<TestModel, Int> {
        BoundLens<TestModel, Int>(instance: self, lens: TestModel.someCountLens)
    }
    static var urlsLens: Lens<TestModel, [URL]?> = {
        Lens<TestModel, [URL]?>(
            get: { $0.urls },
            set: { urls, _value in
                TestModel(id: _value.id, index: _value.index, name: _value.name, count: _value.count, someCount: _value.someCount, urls: urls, someModel: _value.someModel)
            }
        )
    }()
    var urlsLens: BoundLens<TestModel, [URL]?> {
        BoundLens<TestModel, [URL]?>(instance: self, lens: TestModel.urlsLens)
    }
    static var someModelLens: Lens<TestModel, TestSomeModel?> = {
        Lens<TestModel, TestSomeModel?>(
            get: { $0.someModel },
            set: { someModel, _value in
                TestModel(id: _value.id, index: _value.index, name: _value.name, count: _value.count, someCount: _value.someCount, urls: _value.urls, someModel: someModel)
            }
        )
    }()
    var someModelLens: BoundLens<TestModel, TestSomeModel?> {
        BoundLens<TestModel, TestSomeModel?>(instance: self, lens: TestModel.someModelLens)
    }
}
extension TestPrimitivesModel {
    static var idLens: Lens<TestPrimitivesModel, Int> = {
        Lens<TestPrimitivesModel, Int>(
            get: { $0.id },
            set: { id, _value in
                TestPrimitivesModel(id: id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var idLens: BoundLens<TestPrimitivesModel, Int> {
        BoundLens<TestPrimitivesModel, Int>(instance: self, lens: TestPrimitivesModel.idLens)
    }
    static var valueLens: Lens<TestPrimitivesModel, Int32?> = {
        Lens<TestPrimitivesModel, Int32?>(
            get: { $0.value },
            set: { value, _value in
                TestPrimitivesModel(id: _value.id, value: value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var valueLens: BoundLens<TestPrimitivesModel, Int32?> {
        BoundLens<TestPrimitivesModel, Int32?>(instance: self, lens: TestPrimitivesModel.valueLens)
    }
    static var doubleValueLens: Lens<TestPrimitivesModel, Double> = {
        Lens<TestPrimitivesModel, Double>(
            get: { $0.doubleValue },
            set: { doubleValue, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var doubleValueLens: BoundLens<TestPrimitivesModel, Double> {
        BoundLens<TestPrimitivesModel, Double>(instance: self, lens: TestPrimitivesModel.doubleValueLens)
    }
    static var decimalValueLens: Lens<TestPrimitivesModel, Decimal> = {
        Lens<TestPrimitivesModel, Decimal>(
            get: { $0.decimalValue },
            set: { decimalValue, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var decimalValueLens: BoundLens<TestPrimitivesModel, Decimal> {
        BoundLens<TestPrimitivesModel, Decimal>(instance: self, lens: TestPrimitivesModel.decimalValueLens)
    }
    static var floatValueLens: Lens<TestPrimitivesModel, Float?> = {
        Lens<TestPrimitivesModel, Float?>(
            get: { $0.floatValue },
            set: { floatValue, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var floatValueLens: BoundLens<TestPrimitivesModel, Float?> {
        BoundLens<TestPrimitivesModel, Float?>(instance: self, lens: TestPrimitivesModel.floatValueLens)
    }
    static var boolValueLens: Lens<TestPrimitivesModel, Bool?> = {
        Lens<TestPrimitivesModel, Bool?>(
            get: { $0.boolValue },
            set: { boolValue, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var boolValueLens: BoundLens<TestPrimitivesModel, Bool?> {
        BoundLens<TestPrimitivesModel, Bool?>(instance: self, lens: TestPrimitivesModel.boolValueLens)
    }
    static var urlValueLens: Lens<TestPrimitivesModel, URL?> = {
        Lens<TestPrimitivesModel, URL?>(
            get: { $0.urlValue },
            set: { urlValue, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var urlValueLens: BoundLens<TestPrimitivesModel, URL?> {
        BoundLens<TestPrimitivesModel, URL?>(instance: self, lens: TestPrimitivesModel.urlValueLens)
    }
    static var decimalValueOptLens: Lens<TestPrimitivesModel, Decimal?> = {
        Lens<TestPrimitivesModel, Decimal?>(
            get: { $0.decimalValueOpt },
            set: { decimalValueOpt, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var decimalValueOptLens: BoundLens<TestPrimitivesModel, Decimal?> {
        BoundLens<TestPrimitivesModel, Decimal?>(instance: self, lens: TestPrimitivesModel.decimalValueOptLens)
    }
    static var someEnumLens: Lens<TestPrimitivesModel, SomeEnum> = {
        Lens<TestPrimitivesModel, SomeEnum>(
            get: { $0.someEnum },
            set: { someEnum, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var someEnumLens: BoundLens<TestPrimitivesModel, SomeEnum> {
        BoundLens<TestPrimitivesModel, SomeEnum>(instance: self, lens: TestPrimitivesModel.someEnumLens)
    }
    static var someEnumOptLens: Lens<TestPrimitivesModel, SomeEnum?> = {
        Lens<TestPrimitivesModel, SomeEnum?>(
            get: { $0.someEnumOpt },
            set: { someEnumOpt, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var someEnumOptLens: BoundLens<TestPrimitivesModel, SomeEnum?> {
        BoundLens<TestPrimitivesModel, SomeEnum?>(instance: self, lens: TestPrimitivesModel.someEnumOptLens)
    }
    static var stringEnumLens: Lens<TestPrimitivesModel, SomeStringEnum> = {
        Lens<TestPrimitivesModel, SomeStringEnum>(
            get: { $0.stringEnum },
            set: { stringEnum, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var stringEnumLens: BoundLens<TestPrimitivesModel, SomeStringEnum> {
        BoundLens<TestPrimitivesModel, SomeStringEnum>(instance: self, lens: TestPrimitivesModel.stringEnumLens)
    }
    static var stringEnumOptLens: Lens<TestPrimitivesModel, SomeStringEnum?> = {
        Lens<TestPrimitivesModel, SomeStringEnum?>(
            get: { $0.stringEnumOpt },
            set: { stringEnumOpt, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: stringEnumOpt, someComplexCodable: _value.someComplexCodable)
            }
        )
    }()
    var stringEnumOptLens: BoundLens<TestPrimitivesModel, SomeStringEnum?> {
        BoundLens<TestPrimitivesModel, SomeStringEnum?>(instance: self, lens: TestPrimitivesModel.stringEnumOptLens)
    }
    static var someComplexCodableLens: Lens<TestPrimitivesModel, SomeComplexCodable?> = {
        Lens<TestPrimitivesModel, SomeComplexCodable?>(
            get: { $0.someComplexCodable },
            set: { someComplexCodable, _value in
                TestPrimitivesModel(id: _value.id, value: _value.value, doubleValue: _value.doubleValue, decimalValue: _value.decimalValue, floatValue: _value.floatValue, boolValue: _value.boolValue, urlValue: _value.urlValue, decimalValueOpt: _value.decimalValueOpt, someEnum: _value.someEnum, someEnumOpt: _value.someEnumOpt, stringEnum: _value.stringEnum, stringEnumOpt: _value.stringEnumOpt, someComplexCodable: someComplexCodable)
            }
        )
    }()
    var someComplexCodableLens: BoundLens<TestPrimitivesModel, SomeComplexCodable?> {
        BoundLens<TestPrimitivesModel, SomeComplexCodable?>(instance: self, lens: TestPrimitivesModel.someComplexCodableLens)
    }
}
extension TestRNModel {
    static var idLens: Lens<TestRNModel, Int> = {
        Lens<TestRNModel, Int>(
            get: { $0.id },
            set: { id, _value in
                TestRNModel(id: id, name: _value.name, owner: _value.owner)
            }
        )
    }()
    var idLens: BoundLens<TestRNModel, Int> {
        BoundLens<TestRNModel, Int>(instance: self, lens: TestRNModel.idLens)
    }
    static var nameLens: Lens<TestRNModel, String> = {
        Lens<TestRNModel, String>(
            get: { $0.name },
            set: { name, _value in
                TestRNModel(id: _value.id, name: name, owner: _value.owner)
            }
        )
    }()
    var nameLens: BoundLens<TestRNModel, String> {
        BoundLens<TestRNModel, String>(instance: self, lens: TestRNModel.nameLens)
    }
    static var ownerLens: Lens<TestRNModel, TestSomeModel> = {
        Lens<TestRNModel, TestSomeModel>(
            get: { $0.owner },
            set: { owner, _value in
                TestRNModel(id: _value.id, name: _value.name, owner: owner)
            }
        )
    }()
    var ownerLens: BoundLens<TestRNModel, TestSomeModel> {
        BoundLens<TestRNModel, TestSomeModel>(instance: self, lens: TestRNModel.ownerLens)
    }
}
extension TestRRModel {
    static var idLens: Lens<TestRRModel, Int> = {
        Lens<TestRRModel, Int>(
            get: { $0.id },
            set: { id, _value in
                TestRRModel(id: id, name: _value.name, owner: _value.owner, user: _value.user)
            }
        )
    }()
    var idLens: BoundLens<TestRRModel, Int> {
        BoundLens<TestRRModel, Int>(instance: self, lens: TestRRModel.idLens)
    }
    static var nameLens: Lens<TestRRModel, String> = {
        Lens<TestRRModel, String>(
            get: { $0.name },
            set: { name, _value in
                TestRRModel(id: _value.id, name: name, owner: _value.owner, user: _value.user)
            }
        )
    }()
    var nameLens: BoundLens<TestRRModel, String> {
        BoundLens<TestRRModel, String>(instance: self, lens: TestRRModel.nameLens)
    }
    static var ownerLens: Lens<TestRRModel, TestSomeModel?> = {
        Lens<TestRRModel, TestSomeModel?>(
            get: { $0.owner },
            set: { owner, _value in
                TestRRModel(id: _value.id, name: _value.name, owner: owner, user: _value.user)
            }
        )
    }()
    var ownerLens: BoundLens<TestRRModel, TestSomeModel?> {
        BoundLens<TestRRModel, TestSomeModel?>(instance: self, lens: TestRRModel.ownerLens)
    }
    static var userLens: Lens<TestRRModel, TestERRModel?> = {
        Lens<TestRRModel, TestERRModel?>(
            get: { $0.user },
            set: { user, _value in
                TestRRModel(id: _value.id, name: _value.name, owner: _value.owner, user: user)
            }
        )
    }()
    var userLens: BoundLens<TestRRModel, TestERRModel?> {
        BoundLens<TestRRModel, TestERRModel?>(instance: self, lens: TestRRModel.userLens)
    }
}
extension TestSimpleModel {
    static var titleLens: Lens<TestSimpleModel, String> = {
        Lens<TestSimpleModel, String>(
            get: { $0.title },
            set: { title, _value in
                TestSimpleModel(title: title, count: _value.count)
            }
        )
    }()
    var titleLens: BoundLens<TestSimpleModel, String> {
        BoundLens<TestSimpleModel, String>(instance: self, lens: TestSimpleModel.titleLens)
    }
    static var countLens: Lens<TestSimpleModel, Int> = {
        Lens<TestSimpleModel, Int>(
            get: { $0.count },
            set: { count, _value in
                TestSimpleModel(title: _value.title, count: count)
            }
        )
    }()
    var countLens: BoundLens<TestSimpleModel, Int> {
        BoundLens<TestSimpleModel, Int>(instance: self, lens: TestSimpleModel.countLens)
    }
}
extension TestSomeModel {
    static var userIdLens: Lens<TestSomeModel, Int> = {
        Lens<TestSomeModel, Int>(
            get: { $0.userId },
            set: { userId, _value in
                TestSomeModel(userId: userId, userName: _value.userName, userAvatar: _value.userAvatar, title: _value.title, count: _value.count, nestedModel: _value.nestedModel)
            }
        )
    }()
    var userIdLens: BoundLens<TestSomeModel, Int> {
        BoundLens<TestSomeModel, Int>(instance: self, lens: TestSomeModel.userIdLens)
    }
    static var userNameLens: Lens<TestSomeModel, String> = {
        Lens<TestSomeModel, String>(
            get: { $0.userName },
            set: { userName, _value in
                TestSomeModel(userId: _value.userId, userName: userName, userAvatar: _value.userAvatar, title: _value.title, count: _value.count, nestedModel: _value.nestedModel)
            }
        )
    }()
    var userNameLens: BoundLens<TestSomeModel, String> {
        BoundLens<TestSomeModel, String>(instance: self, lens: TestSomeModel.userNameLens)
    }
    static var userAvatarLens: Lens<TestSomeModel, String> = {
        Lens<TestSomeModel, String>(
            get: { $0.userAvatar },
            set: { userAvatar, _value in
                TestSomeModel(userId: _value.userId, userName: _value.userName, userAvatar: userAvatar, title: _value.title, count: _value.count, nestedModel: _value.nestedModel)
            }
        )
    }()
    var userAvatarLens: BoundLens<TestSomeModel, String> {
        BoundLens<TestSomeModel, String>(instance: self, lens: TestSomeModel.userAvatarLens)
    }
    static var titleLens: Lens<TestSomeModel, String?> = {
        Lens<TestSomeModel, String?>(
            get: { $0.title },
            set: { title, _value in
                TestSomeModel(userId: _value.userId, userName: _value.userName, userAvatar: _value.userAvatar, title: title, count: _value.count, nestedModel: _value.nestedModel)
            }
        )
    }()
    var titleLens: BoundLens<TestSomeModel, String?> {
        BoundLens<TestSomeModel, String?>(instance: self, lens: TestSomeModel.titleLens)
    }
    static var countLens: Lens<TestSomeModel, Int> = {
        Lens<TestSomeModel, Int>(
            get: { $0.count },
            set: { count, _value in
                TestSomeModel(userId: _value.userId, userName: _value.userName, userAvatar: _value.userAvatar, title: _value.title, count: count, nestedModel: _value.nestedModel)
            }
        )
    }()
    var countLens: BoundLens<TestSomeModel, Int> {
        BoundLens<TestSomeModel, Int>(instance: self, lens: TestSomeModel.countLens)
    }
    static var nestedModelLens: Lens<TestSomeModel, TestNestedModel?> = {
        Lens<TestSomeModel, TestNestedModel?>(
            get: { $0.nestedModel },
            set: { nestedModel, _value in
                TestSomeModel(userId: _value.userId, userName: _value.userName, userAvatar: _value.userAvatar, title: _value.title, count: _value.count, nestedModel: nestedModel)
            }
        )
    }()
    var nestedModelLens: BoundLens<TestSomeModel, TestNestedModel?> {
        BoundLens<TestSomeModel, TestNestedModel?>(instance: self, lens: TestSomeModel.nestedModelLens)
    }
}
