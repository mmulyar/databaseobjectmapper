import XCTest
import RealmSwift
import DatabaseObjectsMapper
@testable import DatabaseObjectsMapper_Example


class RealmPerformanceTests: XCTestCase {

    lazy var service: RealmService = {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,

            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: {
                _, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if oldSchemaVersion < 3 {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
            },
            deleteRealmIfMigrationNeeded: true)

        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config

        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        // swiftlint:disable force_try
        let _ = try! Realm()
        // swiftlint:enable force_try

        return RealmService()
    }()

    var token: DatabaseUpdatesToken?

    override func setUp() {
        super.setUp()
        service.deleteAll(sync: true)
    }

    override func tearDown() {
        service.deleteAll(sync: true)
        super.tearDown()
    }

    func testSimpleSavePerformance() {
        let testModel = TestSomeModel(userId: 2, userName: "ki", userAvatar: "rw", title: "pl", count: 2, nestedModel: nil)

        measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
            startMeasuring()
            service.save(models: Array(0..<1000).map { testModel.userIdLens.set($0) }, sync: true)
            stopMeasuring()
        }
    }

    func testDefaultContainerSavePerformance() {
        let testModel = TestModel(id: 1, index: 3, name: "fr", count: 3, someCount: 4, urls: nil, someModel: nil)

        measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
            startMeasuring()
            service.save(models: Array(0..<1000).map { testModel.idLens.set($0) }, sync: true)
            stopMeasuring()
        }
    }

    func testPrimitivesSavePerformance() {
        let testModel = TestPrimitivesModel(id: 1,
                                            value: 20,
                                            doubleValue: 3.02,
                                            decimalValue: 2.75,
                                            floatValue: nil,
                                            boolValue: true,
                                            urlValue: URL(string: "https://google.com"),
                                            decimalValueOpt: 1.35,
                                            someEnum: .secondCase,
                                            someEnumOpt: .thirdCase,
                                            stringEnum: .firstCase,
                                            stringEnumOpt: .secondCase,
                                            someComplexCodable: SomeComplexCodable(key: "key", index: 24, link: .profile(399), decimal: 12.03))

        measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
            startMeasuring()
            service.save(models: Array(0..<1000).map { testModel.idLens.set($0) }, sync: true)
            stopMeasuring()
        }
    }

    func testCollectionsSavePerformance() {
        let codable = SomeCodable(key: "k", index: 4)
        let persistable = SomePersistable(persistedValue: 3)
        let url = URL(string: "https://google.com")!
        let testModel = TestCollectionsModel(id: 1,
                                             strings: ["one", "two"],
                                             intValues: [0, 3],
                                             doubleValues: nil,
                                             dates: [Date()],
                                             codable: [codable],
                                             persistable: [persistable],
                                             urls: [url],
                                             dict: ["key3": persistable],
                                             anotherDict: [codable: .thirdCase],
                                             set: [url],
                                             anotherSet: [codable],
                                             someEnum: [.thirdCase],
                                             someList: ["Test", "Test1"],
                                             codableEnums: [.chat(32), .program(22)],
                                             codableModels: [],
                                             optionalCodableModels: [])

        measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
            startMeasuring()
            service.save(models: Array(0..<1000).map { testModel.idLens.set($0) }, sync: true)
            stopMeasuring()
        }
    }

    func testRelationsSavePerformance() {
        let subModel = TestSomeModel(userId: 2, userName: "ki", userAvatar: "rw", title: "pl", count: 2, nestedModel: nil)
        let testModel = TestRRModel(id: 1, name: "ll", owner: subModel, user: nil)

        measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
            startMeasuring()
            service.save(models: Array(0..<1000).map { testModel.idLens.set($0) }, sync: true)
            stopMeasuring()
        }
    }

    func testUpdatePerformance() {
        let testModel = TestPrimitivesModel(id: 1,
                                            value: 20,
                                            doubleValue: 3.02,
                                            decimalValue: 2.56,
                                            floatValue: nil,
                                            boolValue: true,
                                            urlValue: URL(string: "https://yahoo.com"),
                                            decimalValueOpt: nil,
                                            someEnum: .secondCase,
                                            someEnumOpt: .thirdCase,
                                            stringEnum: .firstCase,
                                            stringEnumOpt: .secondCase,
                                            someComplexCodable: SomeComplexCodable(key: "key", index: 78, link: .chat(226), decimal: 1.43))

        service.save(models: [testModel])

        let expectation = XCTestExpectation()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
                self.startMeasuring()
                let batch = self.service.startBatchService()
                for _ in 0..<100 {
                    batch.update(modelOf: TestPrimitivesModel.self,
                                        with: testModel.id,
                                        updates: ["doubleValue": 5.0,
                                                  "decimalValue": 23.0329,
                                                  "someEnum": SomeEnum.firstCase,
                                                  "someEnumOpt": nil,
                                                  "someComplexCodable": SomeComplexCodable(key: "key", index: 28, link: .chat(17), decimal: 2.43),
                                                  "decimalValueOpt": 25.2149,
                                                  "stringEnumOpt": SomeStringEnum.thirdCase,
                                                  "urlValue": URL(string: "https://google.com")], sync: true)
                }
                batch.commitBatchWrites(sync: true)
                self.stopMeasuring()
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10)
    }

    func testSimpleFetchPerformance() {
        let testModel = TestSomeModel(userId: 2, userName: "ki", userAvatar: "rw", title: "pl", count: 2, nestedModel: nil)

        service.save(models: Array(0..<1000).map { testModel.userIdLens.set($0) })

        let expectation = XCTestExpectation()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
                self.startMeasuring()
                let fetched: [TestSomeModel] = self.service.syncFetch()
                self.stopMeasuring()
                XCTAssertTrue(fetched.count == 1000)
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10)
    }


    func testDefaultContainerFetchPerformance() {
        let testModel = TestModel(id: 1, index: 3, name: "fr", count: 3, someCount: 4, urls: nil, someModel: nil)

        service.save(models: Array(0..<1000).map { testModel.idLens.set($0) })

        let expectation = XCTestExpectation()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
                self.startMeasuring()
                let fetched: [TestModel] = self.service.syncFetch()
                self.stopMeasuring()
                XCTAssertTrue(fetched.count == 1000)
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10)
    }

    func testCollectionsFetchPerformance() {
        let codable = SomeCodable(key: "k", index: 4)
        let persistable = SomePersistable(persistedValue: 3)
        let url = URL(string: "https://google.com")!
        let testModel = TestCollectionsModel(id: 1,
                                             strings: ["one", "two"],
                                             intValues: [0, 3],
                                             doubleValues: nil,
                                             dates: [Date()],
                                             codable: [codable],
                                             persistable: [persistable],
                                             urls: [url],
                                             dict: ["key3": persistable],
                                             anotherDict: [codable: .thirdCase],
                                             set: [url],
                                             anotherSet: [codable],
                                             someEnum: [.thirdCase],
                                             someList: ["Test", "Test1"],
                                             codableEnums: [.chat(32), .program(22)],
                                             codableModels: [],
                                             optionalCodableModels: nil)

        service.save(models: Array(0..<1000).map { testModel.idLens.set($0) })

        let expectation = XCTestExpectation()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
                self.startMeasuring()
                let fetched: [TestCollectionsModel] = self.service.syncFetch()
                self.stopMeasuring()
                XCTAssertTrue(fetched.count == 1000)
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10)
    }

    func testRelationFetchPerformance() {
        let subModel = TestSomeModel(userId: 2, userName: "ki", userAvatar: "rw", title: "pl", count: 2, nestedModel: nil)
        let testModel = TestRRModel(id: 1, name: "ll", owner: subModel, user: nil)

        service.save(models: Array(0..<1000).map { testModel.idLens.set($0) })

        let expectation = XCTestExpectation()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.measureMetrics(XCTestCase.defaultPerformanceMetrics, automaticallyStartMeasuring: false) {
                self.startMeasuring()
                let fetched: [TestRRModel] = self.service.syncFetch()
                self.stopMeasuring()
                XCTAssertTrue(fetched.count == 1000)
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10)
    }
}
