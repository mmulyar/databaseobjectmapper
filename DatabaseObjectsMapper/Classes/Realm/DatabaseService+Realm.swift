//
// Created by Mikhail Mulyar on 07/01/2018.
// Copyright (c) 2018 Mikhail Mulyar. All rights reserved.
//

import Realm
import RealmSwift


public extension DatabaseMappable where Container: AnyRealmObject {
    static func mappable(for container: Container) throws -> Self {
        guard let mappable = self.init(container.encodedValue, decoder: .realmDecoder) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "Mappable not created"))
        }
        return mappable
    }

    func container(with userInfo: Any?) throws -> Container {
        try realmObject(with: userInfo)
    }

    func realmObject(with userInfo: Any?) throws -> Container {
        let object = Container()
        update(object)
        return object
    }

    func update(_ container: Container) {
        Self.update(container, updates: encodedValue(with: .realmEncoder), relationUpdates: encodedValue(with: .realmRelationsEncoder))
        updateId(for: container)
    }

    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        updateProperties(for: container, updates: updates)
        updateToOneRelationships(for: container, updates: relationUpdates)
    }

    static func partialUpdate(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        container.partialUpdate(updates)
        updateToOneRelationships(for: container, updates: relationUpdates)
    }
}


public extension DatabaseMappable where Container: AnyRealmObject & SharedDatabaseContainer {
    func container(with userInfo: Any?) throws -> Container {
        try realmObject(with: userInfo)
    }

    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        container.typeName = typeName
        updateProperties(for: container, updates: updates)
        updateToOneRelationships(for: container, updates: relationUpdates)
    }

    static func internalPredicate() -> NSPredicate? {
        NSPredicate(format: "typeName == %@", argumentArray: [typeName])
    }
}


public extension UniquelyMappable where Container: AnyRealmObject {
    func existingContainer(with userInfo: Any?) throws -> AnyDatabaseContainer? where Container: Object {
        guard let realm = (userInfo as? Realm) else { return nil }
        return realm.object(ofType: Container.self, forPrimaryKey: objectKeyValue)
    }

    func containerSkippingRelations(with userInfo: Any?) throws -> Container {
        let object = Container()
        Self.updateSkippingRelations(object, updates: encodedValue(with: .realmEncoder))
        updateId(for: object)
        return object
    }

    static func updateSkippingRelations(_ container: Container, updates: [String: Any?]) {
        updateProperties(for: container, updates: updates)
    }

    func update(_ container: Container) {
        Self.update(container, updates: encodedValue(with: .realmEncoder), relationUpdates: encodedValue(with: .realmRelationsEncoder))
        updateId(for: container)
    }

    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        updateProperties(for: container, updates: updates)
        updateToOneRelationships(for: container, updates: relationUpdates)
    }
}

private extension UniquelyMappable where Container: AnyRealmObject {
    func updateId(for container: Container) {
        guard let keyPath = container.objectSchema.primaryKeyProperty?.name,
              container.realm == nil
        else { return }
        container.setValue(objectKeyValue, forKey: keyPath)
    }
}


public extension UniquelyMappable where Container: AnyRealmObject & SharedDatabaseContainer {
    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        container.typeName = typeName
        updateProperties(for: container, updates: updates)
        updateToOneRelationships(for: container, updates: relationUpdates)
    }
}

private extension UniquelyMappable where Container: AnyRealmObject & SharedDatabaseContainer {
    func updateId(for container: Container) {
        guard let keyPath = container.objectSchema.primaryKeyProperty?.name,
              container.realm == nil
        else { return }
        container.setValue(objectKeyValue, forKey: keyPath)
    }
}

public extension DirectlyMappable where Container: AnyRealmObject {
    func containerSkippingRelations(with userInfo: Any?) throws -> Container {
        let object = Container()
        updateContainerSkippingRelations(object)
        updateId(for: object)
        return object
    }

    func update(_ container: Container) {
        updateContainerSkippingRelations(container)
        updateContainerRelations(container)
        updateId(for: container)
    }
}

extension DatabaseMappable where Container: AnyRealmObject {
    func updateId(for container: Container) {
        guard let keyPath = container.objectSchema.primaryKeyProperty?.name,
              container.objectSchema.primaryKeyProperty?.type == .string,
              container.realm == nil
        else { return }
        container.setValue(UUID().uuidString, forKey: keyPath)
    }

    static func updateProperties(for container: Container, updates: [String: Any?]) {
        container.encodedValue = updates
    }

    static func updateToOneRelationships(for container: Container, updates: [String: Any?]) {
        let relations = Set(container.objectSchema.properties.filter { $0.objectClassName != nil && !$0.isArray }.map { $0.name })
        guard !relations.isEmpty else { return }
        for relation in relations {
            guard let update = updates[relation] else {
                continue
            }
            guard let value = update as? AnyDatabaseMappable else {
                container[relation] = nil
                continue
            }
            if let oldObject = try? value.existingContainer(with: container.realm) {
                value.update(oldObject)
                container[relation] = oldObject
            } else {
                container[relation] = try? value.container(with: container.realm)
            }
        }
    }
}


typealias ObjCHashable = Hashable & _ObjcBridgeable


public protocol AnyRealmObject: NSObject {
    init()
    var realm: Realm? { get }
    var objectSchema: ObjectSchema { get }
    subscript(key: String) -> Any? { get set }
}


// MARK: - Object + AnyRealmObject
extension Object: AnyRealmObject {}


// MARK: - EmbeddedObject + AnyRealmObject
extension EmbeddedObject: AnyRealmObject {}


public extension DatabaseContainer where Self: AnyRealmObject {
    var encodedValue: [String: Any?] {
        get {
            let properties = objectSchema.properties
            var encoded: [String: Any?] = Dictionary(uniqueKeysWithValues: properties
                    .filter { $0.objectClassName == nil }
                    .compactMap {
                        guard let value = self[$0.name] else {
                            return nil
                        }
                        if $0.isArray,
                           let array = self[$0.name] as? RLMSwiftCollectionBase,
                           let arrayValue = array._rlmCollection.value(forKey: "self") {
                            return ($0.name, arrayValue)
                        }
                        if $0.isSet,
                           let set = self[$0.name] as? RLMSwiftCollectionBase,
                           let setValue = set._rlmCollection.value(forKey: "self") as? NSSet {
                            return ($0.name, setValue.allObjects)
                        }
                        if $0.isMap,
                           let set = self[$0.name] as? RLMSwiftCollectionBase,
                           let dictValue = set._rlmCollection as? RLMDictionary<NSString, AnyObject> {
                            return ($0.name, Dictionary(uniqueKeysWithValues: zip(dictValue.allKeys, dictValue.allValues)))
                        }
                        if $0.type == .data, let data = value as? Data {
                            if let unarchived: [String: Any?] = Dictionary(archive: data) {
                                return ($0.name, unarchived)
                            } else if let unarchived: [Any?] = Array(archive: data) {
                                return ($0.name, unarchived)
                            }
                        }
                        return ($0.name, value)
                    })
            properties.filter { $0.objectClassName != nil && !$0.isArray && !$0.isSet && !$0.isMap && $0.type != .linkingObjects }.forEach {
                if let object = self[$0.name] as? AnyDatabaseContainer {
                    encoded[$0.name] = object.encodedValue
                } else {
                    encoded[$0.name] = nil
                }
            }
            return encoded
        }
        set {
            let keyPath = objectSchema.primaryKeyProperty?.name
            let properties = Dictionary(uniqueKeysWithValues: objectSchema.properties.filter { $0.objectClassName == nil }.map { ($0.name, $0) })

            partialUpdate(newValue)

            properties.filter { newValue[$0.key] == nil && $0.key != keyPath }.forEach { self[$0.key] = nil }
        }
    }

    func partialUpdate(_ updates: [String: Any?]) {
        let keyPath = objectSchema.primaryKeyProperty?.name
        let properties = Dictionary(uniqueKeysWithValues: objectSchema.properties.filter { $0.objectClassName == nil }.map { ($0.name, $0) })
        updates.forEach {
            guard $0 != keyPath, let property = properties[$0] else { return }
            if property.type == .data {
                // Processing codable properties
                if let dictValue = $1 as? [String: Any?] {
                    self[$0] = dictValue.archived
                } else if let arrayValue = $1 as? [Any?] {
                    self[$0] = arrayValue.archived
                } else {
                    self[$0] = $1
                }
            } else if property.isArray,
                      property.objectClassName == nil,
                      let rlmArray = self[$0] as? RLMArray<NSObject>,
                      let array = $1 as? NSArray {
                rlmArray.removeAllObjects()
                rlmArray.addObjects(array)
            } else if property.isSet,
                      let rlmSet = self[$0] as? RLMSet<NSObject>,
                      let array = $1 as? [NSObject] {
                rlmSet.removeAllObjects()
                rlmSet.addObjects(array as NSArray)
            } else if property.isMap,
                      let rlmDict = self[$0] as? RLMDictionary<NSString, NSObject>,
                      let dict = $1 as? NSDictionary {
                rlmDict.removeAllObjects()
                rlmDict.setDictionary(dict)
            } else if let codable = $1 as? DictionaryCodable {
                self[$0] = codable.encodedValue
            } else {
                self[$0] = $1
            }
        }
    }
}
