//
// Created by Mikhail Mulyar on 2019-03-14.
//

import Foundation
import RealmSwift


protocol BaseProperty {}


// MARK: - Bool + BaseProperty
extension Bool: BaseProperty {}


// MARK: - Int + BaseProperty
extension Int: BaseProperty {}


// MARK: - Int8 + BaseProperty
extension Int8: BaseProperty {}


// MARK: - Int16 + BaseProperty
extension Int16: BaseProperty {}


// MARK: - Int32 + BaseProperty
extension Int32: BaseProperty {}


// MARK: - Int64 + BaseProperty
extension Int64: BaseProperty {}


// MARK: - Float + BaseProperty
extension Float: BaseProperty {}


// MARK: - Double + BaseProperty
extension Double: BaseProperty {}


// MARK: - String + BaseProperty
extension String: BaseProperty {}

public extension DatabaseEncoder {
    static var realmEncoder: DatabaseEncoder {
        let encoder = DatabaseEncoder()
        encoder.valueEncodingStrategy = .asIs(DatabaseMappingEncodingStrategyHelper())
        return encoder
    }
    static var realmRelationsEncoder: DatabaseEncoder {
        let encoder = DatabaseEncoder()
        encoder.valueEncodingStrategy = .asIs(DatabaseMappingRelationsEncodingStrategyHelper())
        encoder.optionalEncodingStrategy = .encodeAsNil
        return encoder
    }
}

public extension DatabaseDecoder {
    static var realmDecoder: DatabaseDecoder {
        let decoder = DatabaseDecoder()
        decoder.valueDecodingStrategy = .asIs(DatabaseMappingDecodingStrategyHelper())
        return decoder
    }
}


private class DatabaseMappingEncodingStrategyHelper: ValueAsIsStrategyHelper {
    func useValueAsIs<T>(_ value: inout Any?, ofType type: T.Type) -> Bool {
        if !(T.self is BaseProperty.Type), T.self is _ObjcBridgeable.Type {
            value = (value as! _ObjcBridgeable)._rlmObjcValue
            return true
        }
        return false
    }
}

private class DatabaseMappingRelationsEncodingStrategyHelper: ValueAsIsStrategyHelper {
    func useValueAsIs<T>(_ value: inout Any?, ofType type: T.Type) -> Bool {
        true
    }
}

private class DatabaseMappingDecodingStrategyHelper: ValueAsIsStrategyHelper {
    func useValueAsIs<T>(_ value: inout Any?, ofType type: T.Type) -> Bool {
        if T.self is Decimal.Type, let decimal = value as? Decimal128 {
            value = Decimal(string: decimal.stringValue)
            return true
        }
        if !(T.self is BaseProperty.Type), let unwrapped = value, let type = (T.self as? _ObjcBridgeable.Type) {
            value = type._rlmFromObjc(unwrapped, insideOptional: false)
            return true
        }
        return false
    }
}
