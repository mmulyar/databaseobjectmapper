//
// Created by Mikhail Mulyar on 07/01/2018.
// Copyright (c) 2018 Mikhail Mulyar. All rights reserved.
//

import CoreData


public extension DatabaseMappable where Container: NSManagedObject {
    func container(with userInfo: Any?) throws -> Container {
        try managedObject(with: userInfo)
    }

    func managedObject(with userInfo: Any?) throws -> Container {
        guard let context = userInfo as? NSManagedObjectContext,
              let entity = NSEntityDescription.entity(forEntityName: Container.entityName, in: context)
        else {
            fatalError("Failed to create object!")
        }
        let object = Container(entity: entity, insertInto: context)
        update(object)
        return object
    }

    func update(_ container: Container) {
        let updates = encodedValue()
        Self.update(container, updates: updates, relationUpdates: updates)
        updateId(for: container)
    }
}


public extension DatabaseMappable where Container: NSManagedObject & SharedDatabaseContainer {
    func container(with userInfo: Any?) throws -> Container {
        try managedObject(with: userInfo)
    }

    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        container.typeName = typeName
        defaultUpdate(container, updates: updates, relationUpdates: relationUpdates)
    }

    static func internalPredicate() -> NSPredicate? {
        NSPredicate(format: "typeName == %@", argumentArray: [typeName])
    }
}


public extension UniquelyMappable where Container: NSManagedObject {
    func existingContainer(with userInfo: Any?) throws -> AnyDatabaseContainer? {
        guard let context = userInfo as? NSManagedObjectContext else { return nil }
        let container: Container? = context.findFirst(with: objectKeyValue)
        return container
    }

    func update(_ container: Container) {
        let updates = encodedValue()
        Self.update(container, updates: updates, relationUpdates: updates)
        updateId(for: container)
    }

    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        defaultUpdate(container, updates: updates, relationUpdates: relationUpdates)
    }

    func containerSkippingRelations(with userInfo: Any?) throws -> Container {
        fatalError("Not implemented")
    }

    static func updateSkippingRelations(_ container: Container, updates: [String: Any?]) {
        fatalError("Not implemented")
    }

    func updateId(for container: Container) {
        guard let keyPath = Container.idKey._kvcKeyPathString, container.entity.attributesByName[keyPath] != nil else { return }
        container.setValue(objectKeyValue, forKey: keyPath)
    }
}


public extension UniquelyMappable where Container: NSManagedObject & SharedDatabaseContainer {
    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        container.typeName = typeName
        defaultUpdate(container, updates: updates, relationUpdates: relationUpdates)
    }
}


public extension DatabaseMappable where Container: NSManagedObject {
    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        defaultUpdate(container, updates: updates, relationUpdates: relationUpdates)
    }

    static func partialUpdate(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        fatalError("Not implemented for CoreData containers")
    }

    static func defaultUpdate(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?]) {
        updateProperties(for: container, updates: updates)
        if let context = container.managedObjectContext {
            updateRelationships(for: container, updates: relationUpdates, in: context)
        }
    }

    func updateId(for container: Container) {
        guard let idPath = CoreDataContainer.Container.idKey._kvcKeyPathString,
              container.entity.attributesByName[idPath]?.attributeType == .stringAttributeType
        else { return }
        container.setValue(UUID().uuidString, forKey: idPath)
    }

    static func updateProperties(for container: Container, updates: [String: Any?]) {
        container.encodedValue = updates
    }

    static func updateRelationships(for container: Container, updates: [String: Any?], in writeContext: NSManagedObjectContext) {
        let relations = Set(container.entity.relationshipsByName.values.filter { !$0.isToMany }.map { $0.name })
        guard !relations.isEmpty else { return }
        for relation in relations {
            guard let value = updates[relation] as? AnyDatabaseMappable else {
                continue
            }
            if let oldObject = try? value.existingContainer(with: writeContext) {
                value.update(oldObject)
                container.setValue(oldObject, forKey: relation)
            } else {
                container.setValue(try? value.container(with: nil), forKey: relation)
            }
        }
    }
}


public extension DatabaseContainer where Self: NSManagedObject {
    var encodedValue: [String: Any?] {
        get {
            var encoded: [String: Any?] = entity.attributesByName.compactMapValues { value(forKey: $0.name) }
            entity.relationshipsByName.values.filter { !$0.isToMany }.forEach {
                if let object = value(forKey: $0.name) as? AnyDatabaseContainer {
                    encoded[$0.name] = object.encodedValue
                }
            }
            return encoded
        }
        set {
//            let keyPath = Container.idKey._kvcKeyPathString
            let properties = Set(entity.attributesByName.map { $0.key })

            newValue.forEach {
//                if $0 != keyPath && properties.contains($0) {
                if properties.contains($0) {
                    setValue($1, forKey: $0)
                }
            }

            properties.filter { newValue[$0] == nil }.forEach { setValue(nil, forKey: $0) }
//            properties.filter { newValue[$0] == nil && $0 != keyPath }.forEach { setValue(nil, forKey: $0) }
        }
    }
}
