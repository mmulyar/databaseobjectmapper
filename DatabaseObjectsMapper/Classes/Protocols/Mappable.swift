//
// Created by Mikhail Mulyar on 20/12/2017.
// Copyright (c) 2017 Mikhail Mulyar. All rights reserved.
//

import Foundation

/// A type that is able to be saved to a database by mapping to database object.
/// Any type conforming to this protocol must also conform to the `Codable` protocol so that it can be converted to and from `JSON`.
public protocol DatabaseMappable: DictionaryCodable, AnyDatabaseMappable {
    associatedtype Container: DatabaseContainer

    /// Creates an instance of a `Container` type from a `DatabaseMappable`.
    /// - parameter userInfo: User info can be passed here.
    func container(with userInfo: Any?) throws -> Container

    /// Creates an instance of a `DatabaseMappable` type for a `Container`.
    /// - parameter object: Container that stores the encoded type.
    static func mappable(for container: Container) throws -> Self

    /// Updates an instance of a `Container` type.
    /// By default uses `func update(_ container: Container, updates: [String: Any?])` passing encoded value
    func update(_ container: Container)

    /// Updates an instance of a `Container` type using updates. Updates expected to contain all fields of object.
    /// Nullifies all values not presented in updates.
    /// - parameter container: Container that should be updated.
    /// - parameter updates: updates dictionary.
    /// - parameter relationUpdates: relation updates dictionary.
    static func update(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?])

    /// Updates an instance of a `Container` type using updates. Only fields listed in updates are changed.
    static func partialUpdate(_ container: Container, updates: [String: Any?], relationUpdates: [String: Any?])

    /// Used for type fetching for example in case when single Container can store multiple different types (used by DatabaseContainerProtocol).
    static func internalPredicate() -> NSPredicate?
}


public extension DatabaseMappable {
    static func mappable(for container: Container) throws -> Self {
        guard let mappable = self.init(container.encodedValue) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "Mappable not created"))
        }
        return mappable
    }

    func update(_ container: Container) {
        let updates = encodedValue()
        Self.update(container, updates: updates, relationUpdates: updates)
    }

    static func internalPredicate() -> NSPredicate? {
        nil
    }

    static var typeName: String {
        String(describing: Self.self)
    }

    static var databaseTypeName: String {
        String(describing: Self.Container.self)
    }
}


public protocol UniquelyMappable: DatabaseMappable, IdentifiableObject where Container: UniqueDatabaseContainer {
    /// The id mapped value. This used when retrieving values from a database. Database type can modify id before using it.
    /// Default containers add type name to key to store models of different types.
    /// Default implementation returns non modified key
    static func idMapping(_ primaryKey: ID) -> Container.ID

    /// Creates an instance of a `Container` type from a `DatabaseMappable` not filling relation objects (both `to one` and `to many`).
    /// - parameter userInfo: User info can be passed here.
    func containerSkippingRelations(with userInfo: Any?) throws -> Container

    /// Updates an instance of a `Container` type using updates not filling relation objects (both `to one` and `to many`).
    static func updateSkippingRelations(_ container: Container, updates: [String: Any?])
}

public extension UniquelyMappable {
    var idValue: ID {
        self[keyPath: Self.idKey]
    }

    var objectKeyValue: Container.ID {
        Self.idMapping(idValue)
    }

    static func idMapping(_ id: ID) -> Container.ID {
        if Container.ID.self == ID.self {
            return id as! Container.ID
        } else if Container.ID.self == String.self {
            return String(id) as! Container.ID
        } else {
            fatalError("Need to implement idMapping for custom types")
        }
    }
}


public extension UniquelyMappable where Container.ID == String, Container: SharedDatabaseContainer {
    static func idMapping(_ id: ID) -> Container.ID {
        Self.typeName + "_" + String(id)
    }
}


/// Helper protocol to support relationships
public protocol AnyDatabaseMappable: Codable {
    func existingContainer(with userInfo: Any?) throws -> AnyDatabaseContainer?
    func container(with userInfo: Any?) throws -> AnyDatabaseContainer
    func update(_ container: AnyDatabaseContainer)
}


public extension DatabaseMappable {
    func existingContainer(with userInfo: Any?) throws -> AnyDatabaseContainer? {
        nil
    }

    func container(with userInfo: Any?) throws -> AnyDatabaseContainer {
        let object: Container = try self.container(with: userInfo)
        return object
    }

    func update(_ container: AnyDatabaseContainer) {
        guard let container = container as? Container else { fatalError("Wrong container type") }
        update(container)
    }
}

/// Helper protocol to minimize performance overhead of mapping models to container and back
public protocol DirectlyMappable: UniquelyMappable {
    // Updates container `to one` relations with data from model
    func updateContainerRelations(_ container: Container)

    // Updates container properties with data from model skipping relations updates (both `to one` and `to many`)
    func updateContainerSkippingRelations(_ container: Container)

    func key(for relation: Relation<some UniquelyMappable>) -> String?

    func key(for relation: EmbeddedRelation<some DatabaseMappable>) -> String?
}
