//
// Created by Mikhail Mulyar on 2019-03-14.
//

import Foundation


public protocol DictionaryCodable: Codable {
    /// Inits self with encoded value.
    init?(_ encodedValue: [String: Any?], decoder: DatabaseDecoder)
    /// Returns encoded value.
    func encodedValue(with encoder: DatabaseEncoder) -> [String: Any?]
    func binaryValue(with encoder: DatabaseEncoder) -> Data

    static func create(with data: Data?, using decoder: DatabaseDecoder) throws -> Self?
}


public extension DictionaryCodable {
    init?(_ encodedValue: [String: Any?], decoder: DatabaseDecoder = DatabaseDecoder()) {
        guard let object = try? decoder.decode(Self.self, from: encodedValue) else { return nil }
        self = object
    }

    func encodedValue(with encoder: DatabaseEncoder = DatabaseEncoder()) -> [String: Any?] {
        (try? encoder.encode(self)) ?? [:]
    }

    func binaryValue(with encoder: DatabaseEncoder = DatabaseEncoder()) -> Data {
        encodedValue(with: encoder).archived
    }

    static func create(with data: Data?, using decoder: DatabaseDecoder) throws -> Self? {
        guard let data else { return nil }
        guard let dict: [String: Any?] = .init(archive: data),
              let result: Self = .init(dict, decoder: decoder)
        else {
            throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "Codable not created"))
        }

        return result
    }
}


public extension DatabaseMappable {
    static func databaseType() -> Container.Type {
        Container.self
    }
}


extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")!
}


extension Collection where Element: Codable {
    func encoded(with encoder: DatabaseEncoder = DatabaseEncoder()) -> [Any?] {
        // fixes issue that DictionaryEncoder can not encode arrays/sets as top level elements
        let dictionary: [String: [Element]] = ["value": Array(self)]
        let result: [String: Any?]? = try? encoder.encode(dictionary)
        return (result?["value"] as? [Any?]) ?? []
    }
}

// MARK: - Array + DictionaryCodable
extension Array: DictionaryCodable where Element: Codable {
    public func binaryValue(with encoder: DatabaseEncoder) -> Data {
        let arr: [Any?] = encoded(with: encoder)
        return arr.archived
    }

    public static func create(with data: Data?, using decoder: DatabaseDecoder) throws -> Self? {
        guard let data else { return nil }
        guard let arr: [Any?] = .init(archive: data),
              let result: MappingBox<Self> = .init(["value": arr], decoder: decoder)
        else {
            throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "Codable not created"))
        }

        return result.value
    }
}

// MARK: - Set + DictionaryCodable
extension Set: DictionaryCodable where Element: Codable {
    public func binaryValue(with encoder: DatabaseEncoder) -> Data {
        let arr: [Any?] = encoded(with: encoder)
        return arr.archived
    }

    public static func create(with data: Data?, using decoder: DatabaseDecoder) throws -> Self? {
        guard let data else { return nil }
        guard let arr: [Any?] = .init(archive: data),
              let result: MappingBox<Self> = .init(["value": arr], decoder: decoder)
        else {
            throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "Codable not created"))
        }

        return result.value
    }
}

// MARK: - Dictionary + DictionaryCodable
extension Dictionary: DictionaryCodable where Key: Codable, Value: Codable {
    func encoded(with encoder: DatabaseEncoder = DatabaseEncoder()) -> [Any?] {
        let dictionary: [String: Self] = ["value": self]
        let result: [String: Any?]? = try? encoder.encode(dictionary)
        return (result?["value"] as? [Any?]) ?? []
    }

    public func binaryValue(with encoder: DatabaseEncoder) -> Data {
        let arr: [Any?] = encoded(with: encoder)
        return arr.archived
    }

    public static func create(with data: Data?, using decoder: DatabaseDecoder) throws -> Self? {
        guard let data else { return nil }
        guard let arr: [Any?] = .init(archive: data),
              let result: MappingBox<Self> = .init(["value": arr], decoder: decoder)
        else {
            throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "Codable not created"))
        }

        return result.value
    }
}

extension Encodable {
    func encoded(with encoder: DatabaseEncoder = DatabaseEncoder()) -> Any? {
        let dictionary: [String: Self] = ["value": self]
        let result: [String: Any?]? = try? encoder.encode(dictionary)
        return result?["value"] ?? nil
    }
}

struct MappingBox<T: Codable>: DictionaryCodable {
    let value: T
}
