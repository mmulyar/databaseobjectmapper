// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DatabaseObjectsMapper",
    platforms: [
        .macOS(.v10_13),
        .iOS(.v12),
        .tvOS(.v12),
        .watchOS(.v4)
    ],
    products: [
        .library(name: "DatabaseObjectsMapper", targets: ["DatabaseObjectsMapper"]),
        .library(name: "RealmSwift", targets: ["RealmSwift"]),
        .library(name: "Realm", targets: ["Realm"]),
    ],
    dependencies: [],
    targets: [
        .binaryTarget(name: "Realm", url: "https://github.com/realm/realm-swift/releases/download/v10.47.0/Realm.spm.zip", checksum: "299022a7db5c1870d4689ea6aa1dfc3213c0bc61b06eabfc610744449ccd38f1"),
        .binaryTarget(name: "RealmSwift", url: "https://github.com/realm/realm-swift/releases/download/v10.47.0/RealmSwift@15.2.spm.zip", checksum: "e2b98a35a02718c185263424874c2b5d7947adf151e18216cd0f939b5ec5bd63"),
        .target(name: "DatabaseObjectsMapper",
                dependencies: [.byName(name: "Realm"), .byName(name: "RealmSwift")],
                path: "DatabaseObjectsMapper/Classes"),
    ],
    swiftLanguageVersions: [.v5]
)
